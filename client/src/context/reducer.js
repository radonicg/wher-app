export default function reducer(state, { type, payload }) {
    switch (type) {
        case 'LOGIN_USER':
            return {
                ...state,
                currentUser: payload,
                isAuth: true
            }
        case 'LOGOUT_USER':
            return {
                ...state,
                isAuth: false,
                currentUser: null,
                draft: null,
                pins: [],
                currentPin: null,
                notifications: [],
                error: payload
            }
        case 'CREATE_DRAFT':
            return {
                ...state,
                currentPin: null,
                draft: payload
            }
        case 'DELETE_DRAFT':
            return {
                ...state,
                draft: null
            }
        case 'DELETE_CURRENT_PIN':
            return {
                ...state,
                currentPin: null
            }
        case 'GET_PINS':
            return {
                ...state,
                pins: payload
            }
        case 'CREATE_PIN':
            const newPin = payload
            const prevPins = state.pins.filter(pin => pin._id !== newPin._id)
            return {
                ...state,
                pins: [...prevPins, newPin]
            }
        case 'SET_PIN':
            return {
                ...state,
                currentPin: payload,
                draft: null
            }
        case 'DELETE_PIN':
            const deletedPin = payload;
            const filteredPins = state.pins.filter(pin => pin._id !== deletedPin._id);
            const filteredNotifications = state.notifications.filter(notification => notification._id !== deletedPin._id);
            if (state.currentPin) {
                const isCurrentPin = deletedPin._id === state.currentPin._id;
                if (isCurrentPin) {
                    return {
                        ...state,
                        pins: filteredPins,
                        notifications: filteredNotifications,
                        currentPin: null
                    };
                }
            }
            return {
                ...state,
                pins: filteredPins
            }
        case 'CREATE_COMMENT':
            const comment = payload
            let updatedCurrentPin;

            const updatedPins = state.pins.map(pin => {
                if (pin._id === comment.pinId) {
                    pin.comments.push(comment);
                    updatedCurrentPin = pin;
                }
                return pin;
            });

            return {
                ...state,
                pins: updatedPins,
                currentPin: updatedCurrentPin
            }
        case 'SET_NOTIFICATIONS':
            return {
                ...state,
                notifications: payload
            }
        case 'CREATE_NOTIFICATION':
            let currentNotifications = state.notifications;
            if (currentNotifications.length >= process.env.REACT_APP_MAX_NOTIFICATIONS) {
                currentNotifications.pop();
            }
            return {
                ...state,
                notifications: [payload, ...currentNotifications]
            }
        case 'SET_VIEWPORT':
            return {
                ...state,
                viewport: payload
            }
        case 'UNSET_ERROR':
            return {
                ...state,
                error: null
            }
        default:
            return state
    }
}