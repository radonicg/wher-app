import { createContext } from 'react';
const jwtDecode = require('jwt-decode');

const INITIAL_VIEWPORT = {
    latitude: 37.7577,
    longitude: -122.4376,
    zoom: 13
}

const initialState = {
    currentUser: null,
    isAuth: false,
    draft: null,
    pins: [],
    currentPin: null,
    notifications: [],
    viewport: INITIAL_VIEWPORT,
    error: null
};

const token = localStorage.getItem('token');

if (token) {
    const { userId, firstname, lastname, picture, username, exp } = jwtDecode(token);
    if (exp * 1000 < Date.now()) {
        localStorage.removeItem('token');
    } else {
        initialState.currentUser = {
            userId,
            firstname,
            lastname,
            picture,
            username
        };
        initialState.isAuth = true;
    }
}


const AppContext = createContext(initialState);
export default AppContext;