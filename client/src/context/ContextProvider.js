import React, { useContext, useReducer } from 'react';
import AppContext from './AppContext';
import reducer from './reducer';
import jwtDecode from 'jwt-decode';

const ContextProvider = props => {
    const initialState = useContext(AppContext);
    const [state, dispatch] = useReducer(reducer, initialState);

    const actions = {
        login: ({ token }) => {
            localStorage.setItem('token', token);
            const { userId, firstname, lastname, picture, username } = jwtDecode(token);
            dispatch({ type: 'LOGIN_USER', payload: { userId, firstname, lastname, picture, username } });
        },
        logout: (message = '') => {
            localStorage.removeItem('token');
            dispatch({ type: 'LOGOUT_USER', payload: message })
        },
        getPins: pins => {
            dispatch({ type: 'GET_PINS', payload: pins });
        },
        createDraft: (draft) => {
            dispatch({ type: 'CREATE_DRAFT', payload: draft });
        },
        setPin: (pin) => {
            dispatch({ type: 'SET_PIN', payload: pin })
        },
        createPin: (pin) => {
            dispatch({ type: "CREATE_PIN", payload: pin });
        },
        deletePin: (pin) => {
            dispatch({ type: 'DELETE_PIN', payload: pin });
        },
        createComment: (comment) => {
            dispatch({ type: "CREATE_COMMENT", payload: comment });
        },
        deleteDraft: () => {
            dispatch({ type: 'DELETE_DRAFT' });
        },
        deleteCurrentPin: () => {
            dispatch({ type: 'DELETE_CURRENT_PIN' });
        },
        setNotifications: (notifications) => {
            dispatch({ type: 'SET_NOTIFICATIONS', payload: notifications });
        },
        createNotification: (notification) => {
            dispatch({ type: 'CREATE_NOTIFICATION', payload: notification });
        },
        setViewport: (coords) => {
            dispatch({ type: 'SET_VIEWPORT', payload: coords });
        },
        unsetError: () => {
            dispatch({ type: 'UNSET_ERROR' });
        }
    }

    return (
        <AppContext.Provider value={{ state, ...actions }}  {...props} />
    );
}

export default ContextProvider;
