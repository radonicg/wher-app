import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ApolloProvider from './graphql/ApolloProvider';
import 'bootstrap/dist/css/bootstrap.min.css';
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
    palette: {
        primary: { main: "#CC7949" },
        secondary: { main: "#FFDBB5" },
        type: "dark"
    }
});


ReactDOM.render(<MuiThemeProvider theme={theme}><ApolloProvider /></MuiThemeProvider>, document.getElementById('root'));