import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import { ApolloProvider as Provider } from '@apollo/react-hooks';
import { split } from 'apollo-link';
import { createHttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import { InMemoryCache } from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import { setContext } from 'apollo-link-context';
import ContextProvider from '../context/ContextProvider';

import App from '../pages/App';
import ProtectedRoute from '../ProtectedRoute';
import Splash from '../pages/Splash';
import Register from '../components/auth/Register';

const authLink = setContext(() => {
    const token = localStorage.getItem('token');

    return {
        headers: {
            Authorization: token ? `Bearer ${token}` : ''
        }
    }
});

const httpLink = createHttpLink({
    uri: process.env.REACT_APP_APOLLO_HTTP_LINK
});

const wsLink = new WebSocketLink({
    uri: process.env.REACT_APP_APOLLO_WS_LINK,
    options: {
        timeout: 60000,
        reconnect: true,
        lazy: true
    }
});

const authMiddleware = {
    applyMiddleware: (options, next) => {
        const token = localStorage.getItem('token');
        options.Authorization = token ? `Bearer ${token}` : '';
        next();
    }
}
wsLink.subscriptionClient.use([authMiddleware]);

const link = split(
    ({ query }) => {
        const definition = getMainDefinition(query);
        return (
            definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
        );
    },
    wsLink,
    authLink.concat(httpLink)
);

const apolloClient = new ApolloClient({
    link,
    cache: new InMemoryCache(),
    defaultOptions: {
        watchQuery: {
            fetchPolicy: 'cache-and-network'
        }
    }
});

const ApolloProvider = () => {
    return (
        <Router>
            <Provider client={apolloClient}>
                <ContextProvider>
                    <Switch>
                        <ProtectedRoute exact path="/" component={App} />
                        <Route path="/register" component={Register} />
                        <Route path="/login" component={Splash} />
                        <Redirect to='/' />
                    </Switch>
                </ContextProvider>
            </Provider>
        </Router>
    );
}
export default ApolloProvider;