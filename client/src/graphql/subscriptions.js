import gql from 'graphql-tag'

export const PIN_ADDED_SUBSCRIPTION = gql`
subscription {
    pinAdded {
        _id
        createdAt
        title
        image
        content
        latitude
        longitude
        author {
            _id
            firstname
            lastname
            picture
        }
        comments {
            text
            createdAt
            author {
                firstname
                lastname
                picture
            }
        } 
    }
}
`

export const PIN_DELETED_SUBSCRIPTION = gql`
subscription {
    pinDeleted {
        _id
    }
}
`

export const COMMENT_ADDED_SUBSCRIPTION = gql`
subscription {
    commentAdded {
        pinId
        text
        createdAt
        author {
            firstname
            lastname
            picture
        }
    }
}
`

export const NOTIFICATION_ADDED_SUBSCRIPTION = gql`
subscription($userId: ID!) {
    notificationAdded(userId: $userId) {
        _id
        title
        createdAt
        latitude
        longitude
        author {
            firstname
            lastname
        }
    }
}
`
