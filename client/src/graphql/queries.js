import gql from 'graphql-tag';

export const LOGIN_USER_QUERY = gql`
query login (
    $username: String!
    $password: String!
    ) {
  login (
      username: $username
      password: $password
      ) {
        token
  }
}`

export const GET_PINS_QUERY = gql`
{
  getPins {
    _id
    createdAt
    title
    image
    content
    latitude
    longitude
    author {
      _id
      firstname
      lastname
      picture
    }
    comments {
      text
      createdAt
      author {
        firstname
        lastname
        picture
      }
    }
  }
}`

export const GET_NOTIFICATIONS_QUERY = gql`
{
  getNotifications {
    _id
    title
    createdAt
    latitude
    longitude
    author {
      firstname
      lastname
    }
  }
}`
