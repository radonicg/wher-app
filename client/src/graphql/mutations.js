import gql from 'graphql-tag';

export const CREATE_USER_MUTATION = gql`
mutation createUser(
    $firstname: String!
    $lastname: String!
    $email: String!
    $picture: String!
    $username: String!
    $password: String!
    ) {
    createUser(
        userInput: {
            firstname: $firstname
            lastname: $lastname
            email: $email
            picture: $picture
            username: $username
            password: $password
        }
    ) {
        token
    }
}
`

export const CREATE_PIN_MUTATION = gql`
mutation($title: String!, $image: String!, $content: String!, $latitude:
    Float!, $longitude: Float!) {
        createPin(input: {
            title: $title,
            image: $image,
            content: $content,
            latitude: $latitude,
            longitude: $longitude
        }) {
            _id
            createdAt
            title
            image
            content
            latitude
            longitude
            author {
                _id
                firstname
                lastname
            }
            comments {
                text
                createdAt
                author {
                    firstname
                    lastname
                    picture
                }
            } 
        }
    }`

export const DELETE_PIN_MUTATION = gql`
mutation($id: ID!) {
   deletePin( pinId: $id) {
       _id
   } 
}
`

export const CREATE_COMMENT_MUTATION = gql`
mutation($id: ID!, $text: String!) {
    createComment(pinId: $id, text: $text) {
        title
    }
}`

export const UPDATE_SEEN_NOTIFICATIONS_MUTATION = gql`
mutation($userId: ID!) {
    updateSeenNotifications(userId: $userId) {
        _id
    }
}`