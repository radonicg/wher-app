import React, { useContext } from "react";
import { Redirect } from 'react-router-dom';
import Login from '../components/auth/Login';
import AppContext from '../context/AppContext';


const Splash = () => {
    const { state } = useContext(AppContext)
    return state.isAuth ? <Redirect to="/" /> : < Login />
};

export default Splash;