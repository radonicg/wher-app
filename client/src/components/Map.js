import React, { useState, useEffect, useContext } from 'react';
import { useLazyQuery, useMutation, useSubscription } from '@apollo/react-hooks';
import ReactMapGL, { NavigationControl, Marker, Popup } from 'react-map-gl';
import AppContext from '../context/AppContext';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/DeleteTwoTone';
import differenceInMinutes from 'date-fns/differenceInMinutes';
import PinIcon from './pin/PinIcon';
import Blog from './Blog';

import { GET_PINS_QUERY } from '../graphql/queries';
import { DELETE_PIN_MUTATION } from '../graphql/mutations';
import { PIN_ADDED_SUBSCRIPTION, PIN_DELETED_SUBSCRIPTION, COMMENT_ADDED_SUBSCRIPTION } from '../graphql/subscriptions';

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
    },
    rootMobile: {
        display: 'flex',
        flexDirection: 'column-reverse'
    },
    navigationControl: {
        position: 'absolute',
        top: 0,
        left: 0,
        margin: '1em'
    },
    deleteIcon: {
        color: 'red'
    },
    popupImage: {
        padding: '0.4em',
        height: 200,
        width: 200,
        objectFit: 'cover'
    },
    popupTab: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
    },

}));

const Map = () => {
    const classes = useStyles();
    const [userPosition, setUserPosition] = useState(null);
    const { state, getPins, createDraft, setPin, createPin, deletePin, createComment, deleteCurrentPin, setViewport, logout } = useContext(AppContext);

    const [loginQuery] = useLazyQuery(GET_PINS_QUERY, {
        onCompleted({ getPins: pins }) {
            getPins(pins);
        },
        onError({ graphQLErrors }) {
            if (graphQLErrors[0].extensions.code === 'UNAUTHENTICATED') {
                logout('Session expired. Sign in again.');
            } else {
                logout('Something went wrong. Sign in and try again.');
            }
        }
    });

    const [deletePinMutation] = useMutation(DELETE_PIN_MUTATION, {
        update() {
            deleteCurrentPin();
        },
        onError({ graphQLErrors }) {
            if (graphQLErrors[0].extensions.code === 'UNAUTHENTICATED') {
                logout('Session expired. Sign in again.');
            } else {
                logout('Something went wrong. Sign in and try again.');
            }
        }
    });

    useSubscription(PIN_DELETED_SUBSCRIPTION, {
        onSubscriptionData({ subscriptionData }) {
            const { pinDeleted } = subscriptionData.data;
            deletePin(pinDeleted);
        }
    });

    useSubscription(PIN_ADDED_SUBSCRIPTION, {
        onSubscriptionData({ subscriptionData }) {
            const { pinAdded } = subscriptionData.data;
            createPin(pinAdded);
        }
    });

    useSubscription(COMMENT_ADDED_SUBSCRIPTION, {
        onSubscriptionData({ subscriptionData }) {
            const { commentAdded } = subscriptionData.data;
            createComment(commentAdded);
        }
    });

    useEffect(() => {
        const pinExists = state.currentPin && state.pins.findIndex(pin => pin._id === state.currentPin._id) > -1
        if (!pinExists) {
            deleteCurrentPin();
        }
        // eslint-disable-next-line
    }, [state.pins.length]);

    useEffect(() => {
        getUserPosition();
        // eslint-disable-next-line
    }, []);

    useEffect(() => {
        loginQuery();
        // eslint-disable-next-line
    }, []);

    const getUserPosition = () => {
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(position => {
                const { latitude, longitude } = position.coords;
                setViewport({ latitude, longitude, zoom: 13 });
                setUserPosition({ latitude, longitude });
            })
        }
    }

    const handleMapClick = (e) => {
        if (e.srcEvent.srcElement.notMapClick) return; // argument has true value only when deleting pin
        if (!e.leftButton) return;
        const [longitude, latitude] = e.lngLat;
        createDraft({ longitude, latitude });
    }

    const highlightNewPin = pin => {
        const isNewPin = differenceInMinutes(Date.now(), Number(pin.createdAt)) <= 30
        return isNewPin ? '#AE65C5' : 'green'
    }

    const handleSelectPin = (pin) => {
        setPin(pin);
    }

    const isAuthUser = () => state.currentUser.userId === state.currentPin.author._id;

    const handleDeletePin = (e) => {
        e.target.notMapClick = true; // this field will be present as e.srcEvent.srcElement.dontClickMap in handleMapClick 
        const variables = { id: state.currentPin._id }
        deletePinMutation({ variables });
    }

    return (
        <div className={classes.root}>
            <Blog />
            <ReactMapGL
                width='100vw'
                height='100vh'
                mapStyle={process.env.REACT_APP_MAPBOX_STYLE}
                mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_KEY}
                {...state.viewport}
                scrollZoom={true}
                onViewportChange={setViewport}
                onClick={handleMapClick}
            >
                <div className={classes.navigationControl}>
                    <NavigationControl onViewportChange={setViewport} />
                </div>

                {/*Pin for user's current position  */}
                {userPosition && (
                    <Marker
                        latitude={userPosition.latitude}
                        longitude={userPosition.longitude}
                        offsetLeft={-19}
                        offsetTop={-37}
                    >
                        <PinIcon size={40} color='#CC7949' />
                    </Marker>
                )}

                {/*Draft pin */}
                {state.draft && (
                    <Marker
                        latitude={state.draft.latitude}
                        longitude={state.draft.longitude}
                        offsetLeft={-19}
                        offsetTop={-37}
                    >
                        <PinIcon size={40} color='#424242' />
                    </Marker>
                )}


                {/*Get pins */}
                {state.pins.map(pin => (
                    <Marker
                        key={pin._id}
                        latitude={pin.latitude}
                        longitude={pin.longitude}
                        offsetLeft={-19}
                        offsetTop={-37}
                    >
                        <PinIcon
                            onClick={() => handleSelectPin(pin)}
                            size={40}
                            color={highlightNewPin(pin)} />
                    </Marker>
                ))}

                {/*Popup Dialog*/}
                {state.currentPin && (
                    <Popup
                        anchor='top'
                        latitude={state.currentPin.latitude}
                        longitude={state.currentPin.longitude}
                        closeOnClick={false}
                        onClose={() => deleteCurrentPin()}
                    >
                        <img
                            className={classes.popupImage}
                            src={state.currentPin.image}
                            alt={state.currentPin.title}
                        />
                        <div className={classes.popupTab}>
                            <Typography>
                                {state.currentPin.latitude.toFixed(3)}, {state.currentPin.longitude.toFixed(3)}
                            </Typography>
                            {isAuthUser() && (
                                <Button onClick={handleDeletePin}>
                                    <DeleteIcon className={classes.deleteIcon} />
                                </Button>
                            )}
                        </div>
                    </Popup>
                )}
            </ReactMapGL>
        </div >
    )
};

export default Map;
