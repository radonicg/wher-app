import React, { useContext, useState } from 'react';
import { withRouter } from 'react-router';
import { useLazyQuery } from '@apollo/react-hooks';
import AppContext from '../../context/AppContext';
import { useForm } from '../../util/formHook';
import { LOGIN_USER_QUERY } from '../../graphql/queries';

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import WarningOutlined from '@material-ui/icons/WarningOutlined';

import logo from '../../images/logo.png';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(20),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    link: {
        justifyContent: 'center',
        '&:hover': {
            color: '#FFFFFF',
            textDecoration: 'none'
        }
    },
    error: {
        padding: '10px 15px',
        marginTop: '20px',
        color: '#CC7949',
        border: '1px solid #CC7949',
        borderRadius: '5px',
        width: '395px',
        maxWidth: '100%'
    }
}));


const Login = () => {
    const classes = useStyles();
    const [errors, setErrors] = useState(null);
    const { state, login, unsetError } = useContext(AppContext);

    const { onChange, onSubmit, values } = useForm(loginCallback, {
        username: '',
        password: ''
    });

    const [loginQuery] = useLazyQuery(LOGIN_USER_QUERY, {
        onCompleted({ login: userData }) {
            login(userData);
        },
        onError({ graphQLErrors }) {
            setErrors(graphQLErrors[0].message);
        },
        variables: {
            ...values
        }
    });

    function loginCallback() {
        loginQuery();
    }

    const checkAndSubmit = event => {
        event.preventDefault();
        unsetError();
        values.username.trim() !== '' && values.password.trim() !== '' ? onSubmit(event) : setErrors('All fields are required.');
    }

    return (
        <div>
            <Container component='main' maxWidth='xs'>
                <CssBaseline />
                <div className={classes.paper}>
                    <img src={logo} alt='logo'></img>
                    <Typography component='h1' variant='h5'>Sign in</Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            variant='outlined'
                            margin='normal'
                            fullWidth
                            id='username'
                            label='Username'
                            name='username'
                            value={values.username}
                            onChange={onChange}
                        />
                        <TextField
                            variant='outlined'
                            margin='normal'
                            fullWidth
                            name='password'
                            label='Password'
                            type='password'
                            id='password'
                            value={values.password} onChange={onChange}
                        />
                        <Button
                            fullWidth
                            variant='contained'
                            color='primary'
                            className={classes.submit}
                            onClick={checkAndSubmit}
                            type='submit'
                        >
                            Sign In
                        </Button>
                        <Grid container className={classes.link}>
                            <Grid item>
                                <Link href='/register' variant='body2' className={classes.link}>
                                    {'Don\'t have an account? Sign Up'}
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                    {errors && <div className={classes.error}><WarningOutlined style={{ width: '20px' }} />&nbsp;&nbsp;&nbsp;{errors}</div>}
                    {state.error && <div className={classes.error}><WarningOutlined style={{ width: '20px' }} />&nbsp;&nbsp;&nbsp;{state.error}</div>}
                </div>
            </Container>
        </div>
    );
};

export default withRouter(Login);