import React, { useContext, useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import AppContext from '../../context/AppContext';
import axios from 'axios';
import { useForm } from '../../util/formHook';
import { CREATE_USER_MUTATION } from '../../graphql/mutations';

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AddAPhotoIcon from '@material-ui/icons/AddAPhotoTwoTone';
import FormLabel from '@material-ui/core/FormLabel';
import PortraitIcon from '@material-ui/icons/CheckCircleOutlineTwoTone';
import WarningOutlined from '@material-ui/icons/WarningOutlined';

import logo from '../../images/logo.png';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    link: {
        justifyContent: 'center',
        '&:hover': {
            color: '#FFFFFF',
            textDecoration: 'none'
        }
    },
    input: {
        display: 'none'
    },
    label: {
        display: 'block !important'
    },
    imageButton: {
        marginTop: '15px',
        border: '1px solid #606060',
        padding: '15px'
    },
    imageWrapper: {
        width: '100%'
    }
}));

const Register = ({ history }) => {
    const classes = useStyles();
    const [errors, setErrors] = useState(null);
    const { login } = useContext(AppContext);

    const { onChange, onSubmit, values } = useForm(registerCallback, {
        firstname: '',
        lastname: '',
        email: '',
        picture: '',
        username: '',
        password: ''
    });

    async function registerCallback() {
        let url = await handleImageUpload()
        onChange({ target: { name: 'picture', value: url } });
        addUser();
    }

    const handleImageUpload = async () => {
        const data = new FormData();
        data.append('file', values.picture);
        data.append('upload_preset', process.env.REACT_APP_USERS_PRESET);
        data.append('cloud_name', process.env.REACT_APP_CLOUD_NAME);

        const result = await axios.post(
            process.env.REACT_APP_CLOUDINARY_URL,
            data
        )

        return result.data.url
    }

    const [addUser] = useMutation(CREATE_USER_MUTATION, {
        update(cache, { data: { createUser: userData } }) {
            login(userData);
            history.push('/');
        },
        onError({ graphQLErrors }) {
            setErrors(graphQLErrors[0].message);
        },
        variables: {
            ...values
        }
    });

    const checkAndSubmit = event => {
        event.preventDefault();
        values.firstname.trim() !== '' &&
            values.lastname.trim() !== '' &&
            values.email.trim() !== '' &&
            values.username.trim() !== '' &&
            values.password.trim() !== '' &&
            values.picture !== '' ? onSubmit(event) : setErrors('All fields are required.');
    }

    return (
        <div>
            <Container component='main' maxWidth='xs'>
                <CssBaseline />
                <div className={classes.paper}>
                    <img src={logo} alt='logo' />
                    <Typography component='h1' variant='h5'>Sign Up</Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            variant='outlined'
                            margin='normal'
                            fullWidth
                            id='firstname'
                            label='Firstname'
                            name='firstname'
                            value={values.firstname}
                            onChange={onChange}
                        />
                        <TextField
                            variant='outlined'
                            margin='normal'
                            fullWidth
                            id='lastname'
                            label='Lastname'
                            name='lastname'
                            value={values.lastname}
                            onChange={onChange}
                        />
                        <TextField
                            variant='outlined'
                            margin='normal'
                            fullWidth
                            id='email'
                            label='Email'
                            name='email'
                            value={values.email}
                            onChange={onChange}
                        />
                        <TextField
                            variant='outlined'
                            margin='normal'
                            fullWidth
                            id='username'
                            label='Username'
                            name='username'
                            value={values.username}
                            onChange={onChange}
                        />
                        <TextField
                            variant='outlined'
                            margin='normal'
                            fullWidth
                            name='password'
                            label='Password'
                            type='password'
                            id='password'
                            value={values.password} onChange={onChange}
                        />
                        <input
                            accept='image/*'
                            id='picture'
                            type='file'
                            name='picture'
                            className={classes.input}
                            onChange={onChange}
                        />
                        <FormLabel htmlFor='picture' className={classes.imageWrapper}>
                            <Button
                                fullWidth
                                style={{ color: values.picture && '#CC7949' }}
                                component='span'
                                size='small'
                                className={classes.imageButton}
                            >
                                {!values.picture ? <AddAPhotoIcon /> : <PortraitIcon />}
                            </Button>
                        </FormLabel>
                        <Button
                            fullWidth
                            variant='contained'
                            color='primary'
                            className={classes.submit}
                            onClick={checkAndSubmit}
                            type='submit'
                        >
                            Sign up
                        </Button>
                        <Grid container className={classes.link}>
                            <Grid item>
                                <Link href='/login' variant='body2' className={classes.link}>
                                    {'Already have an account? Sign In'}
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                    {errors && <div className='error'><WarningOutlined style={{ width: '20px' }} />&nbsp;&nbsp;&nbsp;{errors}</div>}
                </div>
            </Container>
        </div>
    );
};

export default Register;