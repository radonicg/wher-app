import React, { useContext, useState, useEffect } from 'react';
import AppContext from '../context/AppContext';
import { useSubscription } from '@apollo/react-hooks';
import { useMutation, useLazyQuery } from '@apollo/react-hooks';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Button from '@material-ui/core/Button';
import favicon from '../images/favicon.png';
import Notification from './pin/Notification';

import { UPDATE_SEEN_NOTIFICATIONS_MUTATION } from '../graphql/mutations';
import { GET_NOTIFICATIONS_QUERY } from '../graphql/queries';
import { NOTIFICATION_ADDED_SUBSCRIPTION } from '../graphql/subscriptions';

const useStyles = makeStyles(theme => ({
    grow: {
        flexGrow: 1,
        position: 'absolute',
        // top: '0',
        // width: 'calc(100% - 12px)',
        width: '100%',
        height: '65px',
        zIndex: '3',
        // overflowX: 'hidden'
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
        fontFamily: 'Montserrat',
        fontSize: '30px'
    },
    search: {
        fontFamily: 'Montserrat',
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(5),
            width: 'auto',
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
        '&::placeholder': {
            fontFamily: 'Montserrat',
        }
    },
    sectionDesktop: {
        display: 'flex',
        zIndex: '5',
        position: 'absolute',
        right: '20px'
    },
    notifications: {
        position: 'absolute',
        top: '50px',
        right: '100px',
        width: '350px',
        height: '400px',
        background: '#FFFFFF',
        border: '1px solid #DDDDDD',
        borderRadius: '3px',
        display: 'block',
        overflowY: 'auto',
        '&::-webkit-scrollbar': {
            width: '5px',
            height: '5px'
        },
        '&::-webkit-scrollbar-thumb': {
            background: '#888',
            borderRadius: '5px'
        },
        '&::-webkit-scrollbar-track': {
            background: '#DDD',
            borderRadius: '5px'
        }
    },
    notificationsTitle: {
        textAlign: 'center',
        color: '#888',
        fontSize: '20px',
        fontWeight: '500',
        padding: '10px',
        margin: '10px',
        borderBottom: '1px solid #CCC'
    },
    noNotification: {
        color: '#CCC',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '65%'
    },
    signout: {
        textTransform: 'capitalize',
        fontFamily: 'Montserrat',
        marginLeft: '20px',
        fontSize: '16px'
    },
    logo: {
        width: '60px',
        height: '60px',
        marginLeft: '-10px'
    }
}));

const Header = () => {
    const classes = useStyles();
    const [showNotifications, setShowNotifications] = useState(false);
    const [unreadNotifications, setUnreadNotifications] = useState(0);
    const { state, logout, setNotifications, createNotification } = useContext(AppContext);


    useEffect(() => {
        getNotifications();
        // eslint-disable-next-line
    }, []);

    const [getNotifications] = useLazyQuery(GET_NOTIFICATIONS_QUERY, {
        onCompleted({ getNotifications: data }) {
            setNotifications(data);
            setUnreadNotifications(data.length);
        },
        onError({ graphQLErrors }) {
            if (graphQLErrors[0].extensions.code === 'UNAUTHENTICATED') {
                logout('Session expired. Sign in again.');
            } else {
                logout('Something went wrong. Sign in and try again.');
            }
        }
    });

    const [updateSeenNotifications] = useMutation(UPDATE_SEEN_NOTIFICATIONS_MUTATION, {
        update(cache, { data: { updateSeenNotifications: data } }) {
        },
        onError({ graphQLErrors }) {
            if (graphQLErrors[0].extensions.code === 'UNAUTHENTICATED') {
                logout('Session expired. Sign in again.');
            } else {
                logout('Something went wrong. Sign in and try again.');
            }
        },
        variables: {
            userId: state.currentUser.userId
        }
    });

    useSubscription(NOTIFICATION_ADDED_SUBSCRIPTION, {
        onSubscriptionData({ subscriptionData }) {
            const { notificationAdded } = subscriptionData.data;
            if (unreadNotifications < process.env.REACT_APP_MAX_NOTIFICATIONS) {
                setUnreadNotifications(unreadNotifications + 1);
            }
            createNotification(notificationAdded);
        },
        variables: {
            userId: state.currentUser.userId
        }
    });

    const handleNotificationsPopupOpen = () => {
        if (!showNotifications) {
            updateSeenNotifications();
        }
        setShowNotifications(!showNotifications);
        setUnreadNotifications(0);
    }

    const renderNotifications = () => {
        if (state.notifications.length === 0) {
            return <div className={classes.noNotification}>No notifications</div>
        }
        return state.notifications.map((notification, index) => <Notification key={index} notification={notification} />)
    }

    return (
        <div className={classes.grow}>
            <AppBar position='static'>
                <Toolbar>
                    <img src={favicon} alt="favicon" className={classes.logo} />
                    <Typography className={classes.title} variant='h6' noWrap>
                        wher
                    </Typography>
                    <div className={classes.sectionDesktop}>
                        <IconButton aria-label='notifications' color='inherit' onClick={handleNotificationsPopupOpen}>
                            <Badge badgeContent={unreadNotifications} color='secondary'>
                                <NotificationsIcon />
                            </Badge>
                        </IconButton>
                        {showNotifications && <div className={classes.notifications}>
                            <div className={classes.notificationsTitle}>Notifications</div>
                            {renderNotifications()}
                        </div>}
                        <Button color="inherit" className={classes.signout} onClick={() => { logout() }}>Signout</Button>
                    </div>
                </Toolbar>
            </AppBar>
        </div >
    );
}

export default Header;
