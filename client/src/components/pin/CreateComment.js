import React, { useContext } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { CREATE_COMMENT_MUTATION } from '../../graphql/mutations';
import { useForm } from '../../util/formHook';
import AppContext from '../../context/AppContext';

import { makeStyles } from '@material-ui/core';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import ClearIcon from '@material-ui/icons/Clear';
import SendIcon from '@material-ui/icons/Send';
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';
import Divider from '@material-ui/core/Divider';

import { Picker } from 'emoji-mart';
import 'emoji-mart/css/emoji-mart.css';


const useStyles = makeStyles((theme) => ({
    form: {
        display: 'flex',
        alignItems: 'center'
    },
    input: {
        marginLeft: 8,
        flex: 1
    },
    clearButton: {
        padding: 0,
        color: '#FFDBB5'
    },
    sendButton: {
        padding: 0,
        color: theme.palette.secondary.dark
    }
}));


const CreateComment = () => {
    const classes = useStyles();
    const { state, logout } = useContext(AppContext);
    const { onChange, onSubmit, values } = useForm(createCommentCallback, {
        id: state.currentPin._id,
        text: '',
        showEmoji: false,
    });

    async function createCommentCallback() {
        createCommentMutation();
    }

    const [createCommentMutation] = useMutation(CREATE_COMMENT_MUTATION, {
        update() {
            onChange({ target: { name: 'text', value: '' } });
        },
        onError({ graphQLErrors }) {
            if (graphQLErrors[0].extensions.code === 'UNAUTHENTICATED') {
                logout('Session expired. Sign in again.');
            } else {
                logout('Something went wrong. Sign in and try again.');
            }
        },
        variables: {
            ...values
        }
    });

    const handleSubmitComment = (event) => {
        onSubmit(event);
        onChange({ target: { name: 'showEmoji', value: false } });
    }

    const handleEmojiInsert = () => {
        onChange({ target: { name: 'showEmoji', value: !values.showEmoji } });
    }

    const onEmojiClick = (emoji, event) => {
        event.preventDefault();
        event.stopPropagation();

        onChange({ target: { name: 'text', value: values.text + emoji.native } });
    }


    return (
        <>
            <form className={classes.form}>
                <IconButton onClick={() => { onChange({ target: { name: 'text', value: '' } }); }} disabled={!values.text.trim()} className={classes.clearButton}>
                    <ClearIcon />
                </IconButton>
                <InputBase
                    multiline={true}
                    className={classes.input}
                    placeholder='Add Comment'
                    name='text'
                    value={values.text}
                    onChange={onChange}
                />
                <IconButton onClick={handleEmojiInsert} className={classes.sendButton}>
                    <InsertEmoticonIcon />
                </IconButton>
                <IconButton onClick={handleSubmitComment} disabled={!values.text.trim()} className={classes.sendButton}>
                    <SendIcon />
                </IconButton>
            </form>
            <Divider />
            {values.showEmoji &&
                <div>
                    <Picker onClick={onEmojiClick} theme='dark' style={{ marginLeft: '75px' }} />
                </div>
            }
        </>
    )
};

export default CreateComment;
