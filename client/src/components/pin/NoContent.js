import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ExploreIcon from '@material-ui/icons/Explore';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center'
    },
    icon: {
        margin: theme.spacing(1),
    }
}));

const NoContent = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <ExploreIcon className={classes.icon} />
            <Typography
                noWrap
                component='h2'
                variant='h6'
                align='center'
                color='textPrimary'
                gutterBottom
            >
                Click on the map to add a pin.
        </Typography>
        </div>
    )
}

export default NoContent;
