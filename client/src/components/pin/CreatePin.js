import React, { useState, useContext } from 'react';
import { useMutation } from '@apollo/react-hooks';
import AppContext from '../../context/AppContext';
import { CREATE_PIN_MUTATION } from '../../graphql/mutations';
import { useForm } from '../../util/formHook';
import axios from 'axios';

import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import AddAPhotoIcon from '@material-ui/icons/AddAPhotoTwoTone';
import LandscapeIcon from '@material-ui/icons/LandscapeOutlined';
import ClearIcon from '@material-ui/icons/Clear';
import SaveIcon from '@material-ui/icons/SaveTwoTone';
import { useMediaQuery } from "@material-ui/core";


const useStyles = makeStyles((theme) => ({
    form: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        paddingBottom: theme.spacing(1)
    },
    small: {
        paddingTop: '200px'
    },
    contentField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '95%'
    },
    input: {
        display: 'none'
    },
    alignCenter: {
        display: 'flex',
        alignItems: 'center'
    },
    iconLarge: {
        fontSize: 40,
        marginRight: theme.spacing(1)
    },
    leftIcon: {
        fontSize: 20,
        marginRight: theme.spacing(1)
    },
    rightIcon: {
        fontSize: 20,
        marginLeft: theme.spacing(1)
    },
    button: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        marginRight: theme.spacing(1),
        marginLeft: 0
    }
}));

const CreatePin = () => {
    const classes = useStyles();
    const { state, deleteDraft, logout } = useContext(AppContext);
    const [submitting, setSubmitting] = useState(false);
    const small = useMediaQuery('(max-width: 700px)');

    const { onChange, onSubmit, values } = useForm(createPinCallback, {
        title: '',
        content: '',
        image: ''
    });

    async function createPinCallback() {
        let url = await handleImageUpload();
        onChange({ target: { name: 'image', value: url } });
        createPinMutation();
    }

    const [createPinMutation] = useMutation(CREATE_PIN_MUTATION, {
        update() {
            handleDeleteDraft();
        },
        onError({ graphQLErrors }) {
            if (graphQLErrors[0].extensions.code === 'UNAUTHENTICATED') {
                logout('Session expired. Sign in again.');
            } else {
                logout('Something went wrong. Sign in and try again.');
            }
        },
        variables: {
            ...values,
            latitude: state.draft.latitude,
            longitude: state.draft.longitude
        }
    });

    const handleSubmit = async (event) => {
        try {
            setSubmitting(true);
            onSubmit(event);
        } catch (err) {
            setSubmitting(false);
            console.error(err);
        }
    }

    const handleImageUpload = async () => {
        const data = new FormData();
        data.append('file', values.image);
        data.append('upload_preset', process.env.REACT_APP_PINS_PRESET);
        data.append('cloud_name', process.env.REACT_APP_CLOUD_NAME);

        const result = await axios.post(
            process.env.REACT_APP_CLOUDINARY_URL,
            data
        );

        return result.data.secure_url;
    }

    const handleDeleteDraft = () => {
        onChange({ target: { name: 'title', value: '' } });
        onChange({ target: { name: 'image', value: '' } });
        onChange({ target: { name: 'content', value: '' } });
        deleteDraft();
    }

    return (
        <form className={`${classes.form} ${small ? classes.small : ''}`}>
            <Typography
                className={classes.alignCenter}
                component='h2'
                variant='h4'
                color='secondary'
            >
                <LandscapeIcon className={classes.iconLarge} /> Pin location
            </Typography>
            <div>
                <TextField
                    name='title'
                    label='Title'
                    placeholder='Insert pin title'
                    value={values.title}
                    onChange={onChange}
                />
                <input
                    accept='image/*'
                    id='image'
                    type='file'
                    name='image'
                    className={classes.input}
                    onChange={onChange}
                />
                <label htmlFor='image'>
                    <Button
                        style={{ color: values.image && 'green' }}
                        component='span'
                        size='small'
                        className={classes.button}
                    >
                        <AddAPhotoIcon />
                    </Button>
                </label>
            </div>
            <div className={classes.contentField}>
                <TextField
                    name='content'
                    label='Content'
                    multiline
                    rows='6'
                    margin='normal'
                    fullWidth
                    variant='outlined'
                    value={values.content}
                    onChange={onChange}
                />
            </div>
            <div>
                <Button
                    className={classes.button}
                    variant='contained'
                    color='primary'
                    onClick={handleDeleteDraft}
                >
                    <ClearIcon className={classes.leftIcon} />
                 Discard
                </Button>
                <Button
                    type='submit'
                    className={classes.button}
                    variant='contained'
                    color='secondary'
                    disabled={!values.title.trim() || !values.image || !values.content.trim() || submitting}
                    onClick={handleSubmit}
                >
                    <SaveIcon className={classes.rightIcon} />
                    Submit
                </Button>
            </div>
        </form>
    )
};

export default CreatePin;
