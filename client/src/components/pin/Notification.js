import React, { useContext } from 'react';
import distanceInWordsToNow from 'date-fns/formatDistanceToNow';
import AppContext from '../../context/AppContext';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    notification: {
        padding: '10px',
        margin: '10px',
        borderBottom: '1px solid #CCC',
        color: '#424242',
        cursor: 'pointer'
    },

    notificationUser: {
        fontWeight: '500'
    },

    notificationTitle: {
        fontStyle: 'italic'
    },

    notificationTime: {
        color: '#CCC',
        textAlign: 'right'
    }
}));

const Notification = ({ notification: { title, createdAt, latitude, longitude, author: { firstname, lastname } } }) => {
    const classes = useStyles();
    const { setViewport } = useContext(AppContext);


    const handleClick = () => {
        setViewport({ latitude, longitude, zoom: 16 });
    }

    return (
        <div className={classes.notification} onClick={handleClick}>
            <span className={classes.notificationUser}>{firstname} {lastname}</span> has created a new pin with a title: <span className={classes.notificationTitle}>{title}</span>.
            <div className={classes.notificationTime}>{distanceInWordsToNow(Number(createdAt), { includeSeconds: true })} ago</div>
        </div>
    );
};

export default Notification;