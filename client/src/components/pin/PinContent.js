import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppContext from '../../context/AppContext';
import Typography from '@material-ui/core/Typography';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import Avatar from '@material-ui/core/Avatar';
import format from 'date-fns/format';
import CreateComment from '../pin/CreateComment';
import PinComments from '../pin/PinComments';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: '1em 0.5em',
        textAlign: 'center',
        width: '100%',
        paddingTop: '100px'
    },
    avatar: {
        width: '30px',
        height: '30px',
        marginRight: '10px'
    },
    icon: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1)
    },
    text: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    }
}));

const PinContent = () => {
    const classes = useStyles();
    const { state: { currentPin: { title, author, content, createdAt, comments } } } = useContext(AppContext);

    return (
        <div className={classes.root}>
            <Typography
                component='h2'
                variant='h4'
                color='primary'
                gutterBottom
            >
                {title}
            </Typography>
            <Typography
                className={classes.text}
                component='h3'
                variant='h6'
                color='inherit'
                gutterBottom
            >
                <Avatar src={author.picture} alt='picture' className={classes.avatar} />{author.firstname} {author.lastname}
            </Typography>
            <Typography
                className={classes.text}
                variant='subtitle2'
                color='inherit'
                gutterBottom
            >
                <AccessTimeIcon className={classes.icon} />
                {format(Number(createdAt), 'MMM do, yyyy')}
            </Typography>
            <Typography
                variant='subtitle1'
                gutterBottom
            >
                {content}
            </Typography>

            <CreateComment />
            <PinComments comments={comments} />
        </div>
    )
};

export default PinContent;
