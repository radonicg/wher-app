import React, { useContext } from "react";
import { makeStyles } from '@material-ui/core/styles';
import AppContext from '../context/AppContext';
import NoContent from './pin/NoContent';
import PinContent from './pin/PinContent';
import CreatePin from './pin/CreatePin';
import { Paper, useMediaQuery } from "@material-ui/core";

const useStyles = makeStyles(() => ({
    root: {
        position: 'absolute',
        left: '0',
        zIndex: '1',
        height: '100%',
        width: 500,
        maxWidth: '40%',
        overflowY: "auto",
        display: "flex",
        borderRadius: '0px',
        justifyContent: "center",
        fontFamily: "Montserrat",
        '&::-webkit-scrollbar': {
            width: '10px',
            height: '5px'
        },
        '&::-webkit-scrollbar-thumb': {
            background: '#888',
            borderRadius: '2px'
        },
        '&::-webkit-scrollbar-track': {
            background: '#DDD'
        }
    },
    small: {
        height: '300px',
        width: '100%',
        maxWidth: '100%'
    },
    button: {
        width: '50px',
        height: '50px',
        borderRadius: '100%',
        backgroundColor: '#424242',
        position: 'fixed',
        left: '475px',
        cursor: 'pointer'
    },
    wrapper: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center'
    }
}));

// const blogRef = createRef();
// const buttonRef = createRef();

const Blog = () => {
    const classes = useStyles();
    const { state: { draft, currentPin } } = useContext(AppContext);
    const small = useMediaQuery('(max-width: 700px)');

    let BlogContent;
    if (!draft && !currentPin) {
        BlogContent = NoContent;
    } else if (draft && !currentPin) {
        BlogContent = CreatePin;
    } else if (currentPin && !draft) {
        BlogContent = PinContent;
    }

    return (
        <Paper className={`${classes.root} ${small ? classes.small : ''}`}>
            <BlogContent />
        </Paper>
    );
};

export default Blog;
