const mongoose = require('mongoose');

const PinSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    latitude: {
        type: Number,
        required: true
    },
    longitude: {
        type: Number,
        required: true
    },
    author: {
        type: mongoose.Schema.ObjectId,
        ref: 'User'
    },
    comments: [{
        text: {
            type: String,
            required: true
        },
        createdAt: {
            type: Date,
            default: Date.now
        },
        author: {
            type: mongoose.Schema.ObjectId,
            ref: 'User'
        },
        pinId: {
            type: mongoose.Schema.ObjectId,
            ref: 'Pin'
        }
    }],
}, { timestamps: true });

module.exports = mongoose.model('Pin', PinSchema);