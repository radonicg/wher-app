const { ApolloServer } = require('apollo-server');
const typeDefs = require('./graphql/typeDefs');
const resolvers = require('./graphql/resolvers');
const { PubSub } = require('apollo-server');

const pubSub = new PubSub();

const mongoose = require('mongoose');
require('dotenv').config();

const auth = require('./middleware/auth');

mongoose
    .connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    })
    .then(() => console.log('DB connected!'))
    .catch(err => console.error('Error!', err));

const server = new ApolloServer({
    typeDefs,
    resolvers,
    playground: true,
    context: ({ req, payload }) => {
        return auth({ req, connection: payload, pubSub });
    }
});

server.listen({ port: process.env.PORT || 4000 }).then(({ url }) => {
    console.log(`Server listening on ${url}`);
});