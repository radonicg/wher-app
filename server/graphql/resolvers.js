const { AuthenticationError, withFilter } = require('apollo-server');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const PIN_ADDED = "PIN_ADDED";
const PIN_DELETED = "PIN_DELETED";
const COMMENT_ADDED = "COMMENT_ADDED";
const NOTIFICATION_ADDED = "NOTIFICATION_ADDED";

const Pin = require('../mongodb/models/Pin');
const User = require('../mongodb/models/User')

module.exports = {
    Query: {
        login: async (obj, { username, password }, ctx) => {
            const user = await User.findOne({ username: username });
            if (!user) {
                throw new AuthenticationError('Authentication error. Try again.');
            }
            const isEqual = await bcrypt.compare(password, user.password);
            if (!isEqual) {
                throw new AuthenticationError('Authentication error. Try again.');
            }
            const token = jwt.sign({
                userId: user.id,
                firstname: user.firstname,
                lastname: user.lastname,
                picture: user.picture,
                username: user.username
            }, process.env.JWT_KEY, { expiresIn: '1h' });
            return { token };
        },
        getPins: async (root, args, { isAuth }) => {
            if (isAuth) {
                return await Pin.find({}).populate('author').populate('comments.author');
            } else {
                throw new AuthenticationError('User not authenticated.');
            }
        },
        getNotifications: async (root, args, { isAuth, userId }) => {
            if (isAuth) {
                let user = await User.findOne({ _id: userId });
                return await Pin.find({ $and: [{ createdAt: { $gt: user.lastSeenNotifications } }, { author: { $ne: userId } }] }).populate('author').sort({ 'createdAt': -1 }).limit(Number(process.env.MAX_NOTIFICATIONS));
            } else {
                throw new AuthenticationError('User not authenticated.');
            }
        }
    },

    Mutation: {
        createUser: async (obj, { userInput }, ctx) => {
            try {
                let foundUser = await User.findOne({ username: userInput.username });
                if (foundUser) {
                    throw new Error('Username is already taken. Try again.');
                }

                const hashedPassword = await bcrypt.hash(userInput.password, 12);
                const user = new User({
                    firstname: userInput.firstname,
                    lastname: userInput.lastname,
                    email: userInput.email,
                    picture: userInput.picture,
                    username: userInput.username,
                    password: hashedPassword
                });
                const result = await user.save();
                const token = jwt.sign({
                    userId: result.id,
                    firstname: result.firstname,
                    lastname: result.lastname,
                    picture: result.picture,
                    username: result.username
                }, process.env.JWT_KEY, { expiresIn: '1h' });
                return { token };
            }
            catch (err) {
                return err;
            }
        },
        createPin: async (obj, args, { isAuth, userId, pubSub }) => {
            if (!isAuth) {
                throw new AuthenticationError('User not authenticated.');
            }
            const newPin = await new Pin({
                ...args.input,
                author: userId
            }).save();
            const pinAdded = await newPin.populate('author').populate('comments.author').execPopulate();
            pubSub.publish(PIN_ADDED, { pinAdded });
            pubSub.publish(NOTIFICATION_ADDED, { notificationAdded: pinAdded });
            return pinAdded;
        },
        deletePin: async (obj, args, { isAuth, pubSub }) => {
            if (!isAuth) {
                throw new AuthenticationError('User not authenticated.');
            }
            const pinDeleted = await Pin.findOneAndDelete({ _id: args.pinId })
            pubSub.publish(PIN_DELETED, { pinDeleted });
            return pinDeleted;
        },
        createComment: async (obj, args, { isAuth, userId, pubSub }) => {
            if (!isAuth) {
                throw new AuthenticationError('User not authenticated.');
            }
            let commentAdded = { text: args.text, author: userId, pinId: args.pinId };
            const pinUpdated = await Pin.findOneAndUpdate(
                { _id: args.pinId },
                { $push: { comments: commentAdded } },
                {
                    useFindAndModify: false,
                    new: true
                }
            ).populate('comments.author').populate('author');
            commentAdded = pinUpdated.comments.pop();
            pubSub.publish(COMMENT_ADDED, { commentAdded });
            return commentAdded;
        },
        updateSeenNotifications: async (obj, args, { isAuth, userId }) => {
            if (!isAuth) {
                throw new AuthenticationError('User not authenticated.');
            }
            return User.findOneAndUpdate({ _id: userId }, { $set: { lastSeenNotifications: new Date() } });
        }
    },

    Subscription: {
        pinAdded: {
            subscribe: withFilter(
                (obj, args, { isAuth, pubSub }) => {
                    if (isAuth) {
                        return pubSub.asyncIterator(PIN_ADDED);
                    } else {
                        throw new AuthenticationError('Authentication error.')
                    }
                },
                (payload, variables, { tokenExp }) => {
                    return new Date(tokenExp * 1000) > Date.now();
                }
            )
        },
        pinDeleted: {
            subscribe: withFilter(
                (obj, args, { isAuth, pubSub }) => {
                    if (isAuth) {
                        return pubSub.asyncIterator(PIN_DELETED);
                    } else {
                        throw new AuthenticationError('Authentication error.')
                    }
                },
                (payload, variables, { tokenExp }) => {
                    return new Date(tokenExp * 1000) > Date.now();
                }
            )
        },
        commentAdded: {
            subscribe: withFilter(
                (obj, args, { isAuth, pubSub }) => {
                    if (isAuth) {
                        return pubSub.asyncIterator(COMMENT_ADDED);
                    } else {
                        throw new AuthenticationError('Authentication error.')
                    }
                },
                (payload, variables, { tokenExp }) => {
                    return new Date(tokenExp * 1000) > Date.now();
                }
            )
        },
        notificationAdded: {
            subscribe: withFilter(
                (obj, args, { isAuth, pubSub }) => {
                    if (isAuth) {
                        return pubSub.asyncIterator(NOTIFICATION_ADDED);
                    } else {
                        throw new AuthenticationError('Authentication error.')
                    }
                },
                (payload, variables, { tokenExp }) => {
                    return new Date(tokenExp * 1000) > Date.now() &&
                        payload.notificationAdded.author._id.toString() !== variables.userId.toString();
                }
            )
        }
    }
}