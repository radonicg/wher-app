const { gql } = require('apollo-server')

module.exports = gql`

    type User {
        _id: ID
        firstname: String
        lastname: String
        email: String
        picture: String
        username: String
    }

    type Pin {
        _id: ID
        createdAt: String
        title: String
        content: String
        image: String
        latitude: Float
        longitude: Float
        author: User
        comments: [Comment]
    }

    type Notification {
        _id: ID
        title: String
        latitude: Float
        longitude: Float
        createdAt: String
        author: User
    }

    type Comment {
        text: String
        createdAt: String
        author: User
        pinId: ID
    }

    input UserInput {
        firstname: String!
        lastname: String!
        email: String!
        picture: String!
        username: String!
        password: String!
    }

    input PinInput {
        title: String
        image: String
        content: String
        latitude: Float
        longitude: Float
    }

    type AuthData {
        token: String!
    }

    type Query {
        login(username: String!, password: String!) : AuthData!
        getPins: [Pin!]
        getNotifications: [Notification!]
    }

    type Mutation {
        createUser(userInput: UserInput): AuthData!
        createPin(input: PinInput!) : Pin
        deletePin(pinId: ID!) : Pin
        createComment(pinId: ID!, text: String!): Pin
        updateSeenNotifications(userId: ID!): User
    }

    type Subscription {
        pinAdded: Pin 
        pinDeleted: Pin
        commentAdded: Comment,
        notificationAdded(userId: ID!): Notification
    }
`