const jwt = require('jsonwebtoken');

module.exports = ({ req, connection, pubSub }) => {
    let authData = req ? req.get('Authorization') : connection.Authorization;

    if (!authData) {
        return {
            isAuth: false,
            pubSub
        }
    }

    const token = authData.split(' ')[1];
    if (!token || token === '') {
        req.isAuth = false;

        return {
            isAuth: false,
            pubSub
        }
    }

    let decodedToken;
    try {
        decodedToken = jwt.verify(token, process.env.JWT_KEY);
    } catch (err) {
        return {
            isAuth: false,
            pubSub
        }
    }

    if (!decodedToken) {
        req.isAuth = false;
        return {
            isAuth: false,
            pubSub
        };
    }
    return {
        isAuth: true,
        userId: decodedToken.userId,
        tokenExp: decodedToken.exp,
        pubSub
    };
}