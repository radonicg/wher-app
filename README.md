# wher

Wher is an application that demonstrates usage of GraphQL in JavaScript applications.

## About

 The application consists of two main parts:

 * Client (frontend)
 * Server (backend)

### Frontend

A frontend part of the application is written using ReactJS and Apollo Client (GraphQL client for React, JavaScript, and native platforms) libraries. Morover, a frontend part of the application uses Mapbox and Cloudinary services to handle creating pins and uploading images.

### Backend

A backend part of the application uses Apollo Server, a server that's compatible with any GraphQL client, including Apollo Client. The application uses MongoDB as well as Mongoose, schema-based solution for modeling application data.

## Installation

To get and run application locally you should run the following commands using Git Bash:

```
git clone https://gitlab.com/radonicg/wher-app.git
cd wher-app

cd client
npm install
npm start

cd ../server
npm install
npm run dev
```

## Features

The application consists of the following features:

* Create new account
* Login to application
* Get all pins created by other users
* Create new pin on chosen location by clicking on the map
* Delete your pins
* Get notifications in real-time
* Keep track of adding and deleting pins in real-time
* Get pin details by clicking on it
* Add comments to selected pins
* Navigate to recently created pins by clicking on notifications



